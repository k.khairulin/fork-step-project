var forkapp = document.getElementsByClassName("header-topics-wrapper")[0];
var header = document.getElementsByClassName("header-wrapper")[0];
var clone = document.getElementsByClassName("header-topics-wrapper")[0].innerHTML;
var callbackClone = document.getElementsByClassName("callback-menu")[0].innerHTML;

var mq = window.matchMedia("(max-width: 480px)");

if (matchMedia) {
	WidthChange(mq);
    mq.addListener(WidthChange);
}

// media query change
function WidthChange(mq) {
	if(mq.matches){
		document.getElementsByClassName("header-topics-wrapper")[0].remove();
		document.getElementsByClassName("input-name")[0].remove();
		document.getElementsByClassName("input-phone")[0].setAttribute('placeholder', 'Email...');
		document.getElementsByClassName("call-button")[0].innerHTML = "SUBSCRIBE";
		var sep = document.createElement('img');
		sep.setAttribute('src', 'img/sep.svg');
		sep.setAttribute('class', 'sep');
		var sandwich = document.createElement('img');
		sandwich.setAttribute('class', 'sandwich');
		sandwich.setAttribute('src', 'img/sandwich.svg');
		sandwich.onclick = menuDropdown;
		header.appendChild(sandwich);
		header.appendChild(sep);
		document.getElementsByClassName("callback-title")[0].innerHTML = 'Subscribe to stay in touch';

    	
	}else {
		if (document.getElementsByClassName("sandwich")[0]) {
			document.getElementsByClassName("sandwich")[0].remove();
		}
		if (document.getElementsByClassName("sep")[0]) {
			document.getElementsByClassName("sep")[0].remove();
		}
		if(document.getElementsByClassName("dropdowns")[0]){
			document.getElementsByClassName("dropdowns")[0].remove();	
		}
		if(!document.getElementsByClassName("header-topics-wrapper")[0]){
			var wrapper = document.createElement('div');
			wrapper.setAttribute('class', 'header-topics-wrapper');
			header.appendChild(wrapper);
			document.getElementsByClassName("header-topics-wrapper")[0].innerHTML = clone;
		}
		document.getElementsByClassName("callback-menu")[0].innerHTML = callbackClone;
		
	}
}

function menuDropdown() {
	var dropDown = document.createElement('div');
	dropDown.setAttribute('class', 'dropdowns');
	var dd1 = document.createElement('div');
	dd1.setAttribute('class', 'dropdown-item');
	var line = document.createElement('img');
	line.setAttribute('src', 'img/border.svg');
	line.setAttribute('class', 'dropdown-left-border');
	dd1.appendChild(line);
	var h1 = document.createElement('h1');
	h1.innerHTML = 'Home';
	h1.setAttribute('class', 'dropdown-home');
	dd1.appendChild(h1);
	var dd2 = document.createElement('div');
	dd2.setAttribute('class', 'dropdown-item');
	dd2.innerHTML = 'Overview';
	var dd3 = document.createElement('div');
	dd3.setAttribute('class', 'dropdown-item');
	dd3.innerHTML = 'About Fork';
	var dd4 = document.createElement('div');
	dd4.setAttribute('class', 'dropdown-item');
	dd4.innerHTML = 'Buying Options';
	var dd5 = document.createElement('div');
	dd5.setAttribute('class', 'dropdown-item');
	dd5.innerHTML = 'Support';
	dropDown.appendChild(dd1);
	dropDown.appendChild(dd2);
	dropDown.appendChild(dd3);
	dropDown.appendChild(dd4);
	dropDown.appendChild(dd5);

	header.appendChild(dropDown);

}